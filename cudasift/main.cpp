#include <iostream>
#include <string>
#include "scale_space.h"
#include "detector.h"
#include "sift.h"
#include "runtime_types.h"

#include "misc.h"
#include "image_scale.h"
#include "gaussian_filter.h"

using namespace std;

int main(int argc, char * argv[]) {

	cout << "Hail to the Sift!" << endl;

	cpu_image * image = extract("C:/Out/et000.jpg", 8);

	//gpu_image * img = upsample(*image, Resampling::Lanczos);

	//export_image(*img, "D:/lanczos.jpg");

	gpu::scale_space * space = new gpu::scale_space(4, 5, image);

	hybrid::detector detector(space);

	std::vector<abstract_keypoint *> p = detector.detect();

	std::cout << "Total points : " << p.size() << std::endl;

	

	//for(int i = 0; i < p.size(); i ++) cout << p.at(i)->orientation.at(0) << std::endl;

	//space->save();

	//resample_kernel_t * k = get_resample_kernel(Resampling::Lanczos);

	//for(int i = 0; i < k->width; i ++) std::cout << k->kernel[i] << " ";

	return 0;
}