#ifndef _IMAGE_SCALE_H_
#define _IMAGE_SCALE_H_

#include "image_types.h"

#define MAX_KERNEL_RADIUS 5
#define MAX_KERNEL_WIDTH (MAX_KERNEL_RADIUS * 2 + 1)
#define MAX_KERNEL_SIZE (MAX_KERNEL_WIDTH * MAX_KERNEL_WIDTH)

static enum Resampling {Linear, Average, Lanczos, Bicubic};

struct resample_kernel_t {

	float * kernel;

	int radius, width;

	resample_kernel_t(float * kernel, int radius, int width) : radius(radius), kernel(kernel), width(width) {}

};

extern "C" resample_kernel_t * get_resample_kernel(Resampling type, int p = 0);

extern "C" resample_kernel_t * get_lanczos_kernel(int a);

extern "C" resample_kernel_t * get_linear_kernel(int r);

extern "C" gpu_image * downsample(gpu_image * image, Resampling type = Resampling::Linear);

extern "C" gpu_image * upsample(gpu_image * image, Resampling type = Resampling::Linear);

#endif