#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>

#include "sift.h"
#include "cuda_configuration.h"
#include "gaussian_filter.h"

__device__ __constant__ int kernel_radius, kernel_width;

__device__ __constant__ float g_kernel[MAX_KERNEL_SIZE];

__global__ void gaussian_blur_kernel(float * in, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;
	
	int t_idx = t_x + t_y * w;

	int x = t_x, y = t_y;

	int idx;

	float value = 0.f, conv_element;

	for(int i = 0; i < kernel_width; i ++) {
		for(int j = 0; j < kernel_width; j ++) {

			x = t_x + j - kernel_radius;
			y = t_y + i - kernel_radius;

			if(x < 0) x = 0;
			if(y < 0) y = 0;

			if(x >= w - 1) x = w - 1;
			if(y >= h - 1) y = h - 1;

			idx = x + y * w;

			value += in[idx] * g_kernel[j + i * kernel_width];
		}
	}

	out[t_idx] = value;
}

__global__ void difference_of_gaussian(float * minuend, float * subtr, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int idx = t_x + t_y * w;

	out[idx] = minuend[idx] - subtr[idx];
}

cuda_launch_options get_gaussian_kernel_configuration(int w, int h, float sigma) {

	cuda_launch_options option;

	//option.blocks = dim3(w, h);
	//option.threads = dim3(KERNEL_WIDTH(KERNEL_RADIUS(sigma)), KERNEL_WIDTH(KERNEL_RADIUS(sigma)));

	/*int w_ratio = 1, h_ratio = 1;

	for(w_ratio = 1; w % w_ratio == 0; w_ratio *= 2);
	for(h_ratio = 1; h % h_ratio == 0; h_ratio *= 2);

	option.blocks = dim3(w / w_ratio, h / h_ratio);
	option.threads = dim3(w_ratio, h_ratio);*/

	option.blocks = dim3(w / 8, h / 8);
	option.threads = dim3(8, 8);

	return option;
}

gpu_image * gaussian_blur(gpu_image * image, float sigma) {

	int w = image->width;
	int h = image->height;

	float * blurred, * h_kernel;

	int h_kernel_radius = get_kernel_radius(sigma), 
		h_kernel_width  = get_kernel_width(h_kernel_radius),
		h_kernel_size   = get_kernel_size(h_kernel_radius);

	h_kernel = get_kernel(sigma);

	cudaMemcpyToSymbol(g_kernel, h_kernel, sizeof(float) * h_kernel_size);

	//std::cout << "Kernel copy : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cudaMemcpyToSymbol(kernel_radius, &h_kernel_radius, sizeof(int));

	//std::cout << "Kernel_radius copy : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cudaMemcpyToSymbol(kernel_width, &h_kernel_width, sizeof(int));

	//std::cout << "Kernel_width : " << cudaGetErrorString(cudaGetLastError()) << std::endl;
	
	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cudaMalloc((void **)&blurred, image->size());

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cuda_launch_options op = get_gaussian_kernel_configuration(w, h, sigma);

	//std::cout << "Launch : blur" << std::endl;
	//std::cout << "Blocks : " << op.blocks.x << " x " << op.blocks.y << std::endl;
	//std::cout << "Threads : " << op.threads.x << " x " << op.threads.y << std::endl;

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	gaussian_blur_kernel<<<op.blocks, op.threads>>>(image->data, blurred, w, h);

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	gpu_image * img = new gpu_image(w, h);

	img->data = blurred;

	return img;
}

cuda_launch_options get_dog_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	cudaDeviceProp device_property;

	cudaGetDeviceProperties(&device_property, 0);

	option.threads = dim3(8, 8);

	option.blocks = dim3(w / option.threads.x, h / option.threads.y);

	return option;

}

gpu_image * difference_of_gaussian(gpu_image * minuend, gpu_image * subtr) {

	int w = minuend->width;
	int h = minuend->height;
	
	float * difference;

	cudaMalloc((void **)&difference, minuend->size());

	cuda_launch_options op = get_dog_kernel_configuration(w, h);

	//std::cout << "Launch : Differ" << std::endl;
	//std::cout << "Blocks : " << op.blocks.x << " x " << op.blocks.y << std::endl;
	//std::cout << "Threads : " << op.threads.x << " x " << op.threads.y << std::endl;

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	difference_of_gaussian<<<op.blocks, op.threads>>>(minuend->data, subtr->data, difference, w, h);

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	gpu_image * image = new gpu_image(w, h);

	image->data = difference;

	return image;
}